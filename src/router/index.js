import Vue from 'vue'
import Router from 'vue-router'
import AppMain from '@/components/AppMain.vue'
import AppAddNew from '@/components/AppAddNew.vue'
import AppItem from '@/components/AppItem.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'AppMain',
      component: AppMain,
      props: true
    },
    {
      path: '/new',
      name: 'AppAddNew',
      component: AppAddNew
    },
    {
      path: '/item/:id',
      name: 'AppItem',
      component: AppItem,
      props: true
    }
  ]
})
