import Vue from 'vue'
import Vuex from 'vuex'
import router from '../router'
import productsRef from '../config.js'

Vue.use(Vuex)
const db = productsRef

export const store = new Vuex.Store({
  state: {
    viewingItem: {},
    slideValue: 200,
    sale: false,
    flashMessage: {
      type: '',
      message: ''
    }
  },
  getters: {
    sliderValue: state => state.slideValue
  },
  mutations: {
    addNewItemSuccess: (state) => {
      state.flashMessage.type = 'success'
      state.flashMessage.message = 'Item Successfully Added'
    },
    clearMessage: state => {
      state.flashMessage.type = ''
      state.flashMessage.message = ''
    },
    clearViewingItem: state => {
      state.viewingItem = {}
    },
    errorMessage: state => {
      state.flashMessage.type = 'error'
      state.flashMessage.message = 'There was an error with your request'
    },
    removeItem: (state) => {
      state.flashMessage.type = 'success'
      state.flashMessage.message = 'Item Successfully Removed'
    },
    setValue: (state, payload) => {
      state.slideValue = payload
    },
    setViewingItem: (state, payload) => {
      state.viewingItem = payload
    },
    switchSale: state => {
      state.sale = !state.sale
    }
  },
  actions: {
    addNewItem (context, payload) {
      db.push(payload).then(data => {
        context.commit('addNewItemSuccess')
        router.push({ name: 'AppMain' })
      }).catch(error => {
        console.log('error adding item', error)
        context.commit('errorMessage')
      })
    },
    clearMessage (context) {
      context.commit('clearMessage')
    },
    getViewingItem (context, payload) {
      db.child(payload).once('value')
        .then((snapshot) => {
          let value = snapshot.val()
          context.commit('setViewingItem', value)
          console.log('viewingItem value', value)
        })
    },
    removeItem (context, payload) {
      db.child(payload['.key']).remove().then(response => {
        context.commit('removeItem')
      }).catch(error => {
        console.log('error removing item', error)
        context.commit('errorMessage')
      })
    },
    setSliderValue (context, payload) {
      context.commit('setValue', payload)
    },
    switchSale (context) {
      context.commit('switchSale')
    }
  }
})
